from game import Game
import sys

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
map = int(argument_list[0])
level = int(argument_list[1])
debug = argument_list[2]

if debug == 'True':
    game = Game(map, level, True)
elif debug == 'False':
    game = Game(map, level, False)
game.flow()
