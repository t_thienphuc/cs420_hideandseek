from colorama import Fore
from colorama import Style
from colorama import init
import copy
import random
from helper import x_move, y_move

seeker_coefficient = 10
hider_coefficient = -10
seeking_range = 10
patient_value = 10
inf = 10000000


class Seeker:
    def __init__(self, x: int, y: int, height: int, width: int, map):
        self.x = x
        self.y = y
        self.map = map
        self.heuristic_map = [[0 for _ in range(width)] for _ in range(height)]
        self.bfs_map = [[0 for _ in range(width)] for _ in range(height)]
        self.bfs_trace = [[None for _ in range(width)] for _ in range(height)]
        self.height = height
        self.width = width
        pass

    # def debug(self):
    #     init()
    #     map_draw = copy.deepcopy(self.heuristic_map)
    #     for i in range(self.height):
    #         for j in range(self.width):
    #             if map_draw[i][j] < 0:
    #                 map_draw[i][j] = f'{Fore.RED}{map_draw[i][j]}{Style.RESET_ALL}'
    #             else:
    #                 map_draw[i][j] = f'{Fore.YELLOW}{map_draw[i][j]}{Style.RESET_ALL}'
    #     for i in range(self.height):
    #         print(' '.join(map_draw[i]))
    #     print('--------------')

    def valid(self, x: int, y: int) -> bool:
        return (x >= 0) and (y >= 0) and (x < self.height) and (y < self.width) and (self.map[x][y] != 1)

    def get_announce(self, hider_index: int, x: int, y: int):
        for i in range(-3, 4):
            for j in range(-3, 4):
                x_next = x + i
                y_next = y + j
                if self.valid(x_next, y_next):
                    self.heuristic_map[x_next][y_next] = min(self.heuristic_map[x_next][y_next], hider_coefficient)
        # self.debug()

    def get_map(self, board):
        self.map = copy.deepcopy(board)
        for x in range(self.height):
            for y in range(self.width):
                if board[x][y] == 5 or board[x][y] == 3:
                    self.heuristic_map[x][y] = seeker_coefficient
                elif board[x][y] == 1:
                    self.heuristic_map[x][y] = inf
                elif board[x][y] == 2:
                    self.heuristic_map[x][y] = -inf
                    for i in range(len(x_move)):
                        x_next = x + x_move[i]
                        y_next = y + y_move[i]
                        if self.valid(x_next, y_next):
                            self.heuristic_map[x_next][y_next] = -inf + 100

        # self.debug()

    def bfs(self):
        queue = [(self.x, self.y)]
        self.bfs_trace = [[None for _ in range(self.width)] for _ in range(self.height)]
        self.bfs_map = [[0 for _ in range(self.width)] for _ in range(self.height)]
        self.bfs_trace[self.x][self.y] = (self.x, self.y)
        self.bfs_map[self.x][self.y] = 0
        i = 0
        while i < len(queue):
            cell = queue[i]
            moves = []
            for move in range(len(x_move)):
                moves.append((x_move[move], y_move[move]))
            random.shuffle(moves)
            for move in moves:
                x_next = cell[0] + move[0]
                y_next = cell[1] + move[1]
                if self.valid(x_next, y_next) and (self.bfs_trace[x_next][y_next] is None):
                    self.bfs_trace[x_next][y_next] = (cell[0], cell[1])
                    self.bfs_map[x_next][y_next] = self.bfs_map[cell[0]][cell[1]] + 1
                    queue.append((x_next, y_next))
            i += 1

    def min_heuristic(self):
        def compare(e):
            return self.bfs_map[e[0]][e[1]]
        nodes = []
        min_heuristic = inf
        for i in range(self.height):
            for j in range(self.width):
                if self.heuristic_map[i][j] < min_heuristic:
                    min_heuristic = self.heuristic_map[i][j]
                    nodes.clear()
                if self.heuristic_map[i][j] == min_heuristic:
                    nodes.append((i, j))
        self.bfs()
        return sorted(nodes, key=compare)

    def cal_1(self):
        min_nodes = self.min_heuristic()[:seeking_range]
        min_node = random.choice(min_nodes)
        if len(min_nodes) > 1:
            self.heuristic_map[min_node[0]][min_node[1]] -= patient_value
        trace = self.bfs_trace[min_node[0]][min_node[1]]
        while (trace[0] != self.x) or (trace[1] != self.y):
            min_node = self.bfs_trace[min_node[0]][min_node[1]]
            trace = self.bfs_trace[min_node[0]][min_node[1]]
        self.x = min_node[0]
        self.y = min_node[1]

    def cal_3(self):
        for i in range(self.height):
            for j in range(self.width):
                if self.heuristic_map[i][j] > 0:
                    self.heuristic_map[i][j] -= 1
                elif self.heuristic_map[i][j] < 0:
                    self.heuristic_map[i][j] += 1
        self.cal_1()


    def cal4(self):
        pass

    def calculate(self, level: int):
        if level < 3:
            self.cal_1()
        if level == 3:
            self.cal_3()
        if level == 4:
            self.cal_4()



