from seeker import Seeker
from hider import Hider
from helper import block
from colorama import Fore
from colorama import Style
from colorama import init
import random
import socket
import copy


class Game:
    def __init__(self, map_id: int, level: int, debug: bool):
        self.level = level
        self.height = 0
        self.width = 0
        self.seeker = None
        self.hiders = []
        self.map = []
        self.pings = []
        self.load_map(map_id)
        self.vision_map = copy.deepcopy(self.map)
        self.score = 20
        self.debug = debug
        self.step = 0

    def send(self, message: str):
        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connected = False
        while not connected:
            try:
                connection.connect((socket.gethostname(), 8000))
                connected = True
            except Exception as e:
                print('Waiting cor connection...')
        msg = message.encode('utf-8')
        total_sent = 0
        while total_sent < len(msg):
            sent = connection.send(msg[total_sent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            total_sent = total_sent + sent
        connection.close()

    def load_map(self, map_id: int):
        file_name = 'map' + str(map_id) + '.txt'
        file = open(file_name, 'r')
        temp = file.readline().split(' ')
        self.height = int(temp[0])
        self.width = int(temp[1])
        self.map = [[0 for _ in range(self.width)] for _ in range(self.height)]
        for i in range(self.height):
            temp = file.readline().split(' ')
            for j in range(self.width):
                tmp = int(temp[j])
                self.map[i][j] = tmp
                if tmp == 3:
                    seeker_i = i
                    seeker_j = j
                elif tmp == 2:
                    self.hiders.append(Hider(i, j, self.height, self.width))
        temp_map = [[0 for _ in range(self.width)] for _ in range(self.height)]
        for i in range(self.height):
            for j in range(self.width):
                temp_map[i][j] = 1 if self.map[i][j] == 1 else 0
        self.seeker = Seeker(seeker_i, seeker_j, self.height, self.width, temp_map)
        for i in range(len(self.hiders)):
            self.hiders[i].index = i
        # thieu obstacle

    def draw_map(self, index: int):
        init()
        map_draw = copy.deepcopy(self.vision_map)
        for i in range(self.height):
            for j in range(self.width):
                if map_draw[i][j] == 3:
                    map_draw[i][j] = f'{Fore.CYAN}3{Style.RESET_ALL}'
                elif map_draw[i][j] == 2:
                    map_draw[i][j] = f'{Fore.GREEN}2{Style.RESET_ALL}'
                elif map_draw[i][j] == 1:
                    map_draw[i][j] = f'{Fore.RED}1{Style.RESET_ALL}'
                elif map_draw[i][j] == 5:
                    map_draw[i][j] = f'{Fore.YELLOW}5{Style.RESET_ALL}'
                else:
                    map_draw[i][j] = str(map_draw[i][j])
        for i in range(self.height):
            print(' '.join(map_draw[i]))
        if len(self.pings) > 0:
            if index == 0:
                for ping in self.pings:
                    print(ping[1], ping[2])
            elif self.pings[-1][0] == index-1:
                print(self.pings[-1][1], self.pings[-1][2])
            else:
                print(-1, -1)
        else:
            print(-1, -1)
        print('-----------------------')

    def send_map(self, pos_x: int, pos_y: int, max_range: int):
        temp_map = copy.deepcopy(self.map)

        def check_block(dx: int, dy: int) -> int:
            for cell in block[dx + 3][dy + 3]:
                cell_x = pos_x + cell[0]
                cell_y = pos_y + cell[1]
                if (temp_map[cell_x][cell_y] == 1) or (temp_map[cell_x][cell_y] == 4):
                    return True
            return False

        for x in range(self.height):
            for y in range(self.width):
                dx = x - pos_x
                dy = y - pos_y
                if abs(dx) > max_range or abs(dy) > max_range:
                    if (temp_map[x][y] == 2) or (temp_map[x][y] == 3):
                        temp_map[x][y] = 0
                elif not check_block(dx, dy):
                    if temp_map[x][y] == 0:
                        temp_map[x][y] = 5
                elif temp_map[x][y] != 1:
                    temp_map[x][y] = 0

        return temp_map

    def graphic_display(self, index: int):
        message = str(self.height) + ' ' + str(self.width) + '\n'
        message += str(index) + '\n'
        for i in range(self.height):
            message += ' '.join(str(temp) for temp in self.vision_map[i]) + '\n'
        if len(self.pings) > 0:
            if index == 0:
                for ping in self.pings:
                    message += f'{ping[1]} {ping[2]} '
            elif self.pings[-1][0] == index - 1:
                message += f'{self.pings[-1][1]} {self.pings[-1][2]}'
            else:
                message += '-1 -1'
        else:
            message += '-1 -1'
        self.send(message)

    def output(self, index: int):
        if not self.debug:
            self.graphic_display(index)
        else:
            self.draw_map(index)

    def hider_move(self, hider: Hider):
        self.map[hider.x][hider.y] = 0
        hider.calculate(self.level)
        self.map[hider.x][hider.y] = 2

    def seeker_move(self):
        self.map[self.seeker.x][self.seeker.y] = 0
        self.seeker.calculate(self.level)
        self.map[self.seeker.x][self.seeker.y] = 3

    def check_win(self) -> bool:
        for hider in self.hiders.copy():
            if (self.seeker.x == hider.x) and (self.seeker.y == hider.y) and (hider.status):
                self.score += 20
                hider.status = False
                self.map[hider.x][hider.y] = 0
        for i in range(self.height):
            for j in range(self.width):
                if self.map[i][j] == 2:
                    return False
        self.vision_map = copy.deepcopy(self.map)
        print("Steps and Scores:",self.step,self.score)
        print('Game end!!!')
        return True

    def flow(self):
        print('Game started!!!')
        print('This is full map: ')
        self.output(-1)
        while not self.check_win():
            for hider in self.hiders:
                if not hider.status:
                    continue
                self.vision_map = self.send_map(hider.x, hider.y, 2 if self.level < 4 else 3)
                print('This is vision of hider ' + str(hider.index) + ' before moving')
                self.output(hider.index+1)
                hider.get_map(self.vision_map)
                self.hider_move(hider)
                randomStep = random.randint(5,10)
                if(self.step % randomStep == 0):
                    x, y = hider.announce()
                    self.pings.append((hider.index, x, y))
                    self.seeker.get_announce(hider.index, x, y)
                self.vision_map = self.send_map(hider.x, hider.y, 2 if self.level < 4 else 3)
                self.output(hider.index + 1)
            self.vision_map = self.send_map(self.seeker.x, self.seeker.y, 3)
            print('This is vision of seeker before moving')
            self.output(0)
            self.step += 1
            self.pings.clear()
            self.seeker.get_map(self.vision_map)
            if self.check_win():
                break
            self.seeker_move()
            self.seeker.get_map(self.vision_map)
            self.vision_map = self.send_map(self.seeker.x, self.seeker.y, 3)
            self.score -= 1
            print("Steps and Scores: ", self.step, self.score)
            self.output(0)
            







