import random
import math
from helper import x_move, y_move, block


class Hider:
    def __init__(self, x: int, y: int, height: int, width: int):
        self.x = x
        self.y = y
        self.index = 0
        self.height = height
        self.width = width
        self.map = None
        self.status = True

    def get_map(self, map):
        self.map = map

    def valid(self, x: int, y: int) -> bool:
        return (x >= 0) and (y >= 0) and (x < self.height) and (y < self.width) and (self.map[x][y] != 1) and (self.map[x][y] != 4) and (self.map[x][y] != 2)

    def check_seeker(self):
        for i in range(self.height):
            for j in range(self.width):
                if self.map[i][j] == 3:
                    return i, j
        return None

    def run_away(self, x: int, y: int):
        def check_block(dx: int, dy: int) -> int:
            for cell in block[dx + 3][dy + 3]:
                cell_x = x + cell[0]
                cell_y = y + cell[1]
                if (self.map[cell_x][cell_y] == 1) or (self.map[cell_x][cell_y] == 4):
                    return True
            return False

        def distance(target_x: int, target_y: int):
            dx = x - target_x
            dy = y - target_y
            return float(math.sqrt((dx*dx)+(dy*dy)))

        is_any_block = False
        nodes = []
        for move in range(len(x_move)):
            x_next = self.x + x_move[move]
            y_next = self.y + y_move[move]
            if self.valid(x_next, y_next):
                dx = x_next - x
                dy = y_next - y
                blocked = check_block(dx, dy)
                if blocked:
                    nodes.clear()
                    is_any_block = True
                if blocked == is_any_block:
                    nodes.append((x_next, y_next))
        if not is_any_block:
            ret = []
            max_value = 0
            for node in nodes:
                dis = distance(node[0], node[1])
                if dis > max_value:
                    ret.clear()
                    max_value = dis
                if max_value == dis:
                    ret.append(node)
            nodes = ret
        return nodes

    def calculate(self, level: int):
        if level > 2:
            seeker = self.check_seeker()
            if seeker:
                nodes = self.run_away(seeker[0], seeker[1])
                node = random.choice(nodes)
                self.x = node[0]
                self.y = node[1]
                return
            while True:
                rand = random.randint(0, len(x_move)-1)
                rand_x = x_move[rand] + self.x
                rand_y = y_move[rand] + self.y
                if self.valid(rand_x, rand_y):
                    self.x = rand_x
                    self.y = rand_y
                    return

    def announce(self):
        while True:
            rand_x = random.randint(-3, 3) + self.x
            rand_y = random.randint(-3, 3) + self.y
            if self.valid(rand_x, rand_y):
                return rand_x, rand_y
